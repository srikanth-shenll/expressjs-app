-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2016 at 04:04 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `expressjs`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `emp_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `phone` int(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`emp_id`, `first_name`, `last_name`, `phone`, `email`, `created_date`) VALUES
(1, 'John', 'DD', 898989896, 'john@gmail.com', '2016-09-01'),
(2, 'Davidddd', 'J', 9090, 'david@gmail.com', '2016-09-01'),
(4, '6666Test6666', 'User', 2147483647, 'test@gmail.com', '2016-09-24'),
(5, 'new srikanth', 'kkk', 2147483647, 'kkk@gmail..com', '2016-09-24'),
(6, 'new srikanth', 'kkk', 2147483647, 'kkk@gmail..com', '2016-09-24'),
(7, 'new srikanth', 'kkk', 2147483647, 'kkk@gmail..com', '2016-09-24'),
(8, 'new srikanth', 'kkk', 2147483647, 'kkk@gmail..com', '2016-09-24'),
(9, 'new srikanth', 'kkk', 2147483647, 'kkk@gmail..com', '2016-09-24'),
(10, 'new srikanth', 'kkk', 2147483647, 'kkk@gmail..com', '2016-09-24'),
(11, 'new srikanth', 'kkk', 2147483647, 'kkk@gmail..com', '2016-09-24'),
(12, 'new srikanth', 'kkk', 2147483647, 'kkk@gmail..com', '2016-09-24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`emp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
